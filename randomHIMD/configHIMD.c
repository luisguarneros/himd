/*
   Microcanonical Molecular Dynamics simulation of a Lennard-Jones fluid
   in a periodic boundary




THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

//	AVOGADRO'S NUMBER
#define		AVOG					6.0225e+23

//	BOLTZMAN'S CONSTANT
#define		BOLTZ					1.3805e-23

#define	MAXNMOL		1000000
#define	NCHX		10

#define frand()      ((float) rand() / (RAND_MAX + 1.0))

double amasssi, amassmolarsi, sigmasi, rhoconst, rhosi, epssi;
  double * rx, * ry, * rz;
  double * vx, * vy, * vz;
  double * fx, * fy, * fz;
  double eta[NCHX][MAXNMOL], etadot[NCHX][MAXNMOL], q[NCHX][MAXNMOL];
  int * ix, * iy, * iz;
  int n=5, N=500;
  double L=0.0;
  double  rc2 = 1.12246, vir, vir_old, V, Vmolar;
  //Paramatros de referencia default
  double Sigma=3.405, Epsilon=119.8, Mass=39.95, Massmolar=39.95, rho=1.0;

  double PE, KE, TE, ecor, ecut, T0=1.0, TE0, T=1.0, Tau=0.2;
  double rr3,dt=0.002, dt2;
  int i,s;
  int nSteps = 10, fSamp=100;
  int use_e_corr=0;
  int unfold = 0;

  char fn[20];
  FILE * out;
  char * wrt_code_str = "w";
  char * init_cfg_file = NULL;
  int NIT=5, NCH=4;
  unsigned long int Seed = 23410981;



/* Writes the coordinates in XYZ format to the output stream fp.
   The integer "z" is the atomic number of the particles, required
   for the XYZ format. The array ix contains the number of x-dir
   periodic boundary crossings a particle has performed; thus,
   the "unfolded" coordinate is rx[i]+ix[i]*L. */
void xyz_out (FILE * fp,
              double * rx, double * ry, double * rz,
              double * vx, double * vy, double * vz,
              int * ix, int * iy, int * iz, double L,
              int N, int z, int put_vel, int unfold) {
  int i;

  fprintf(fp,"%i %i\n\n",N,put_vel);
  for (i=0;i<N;i++) {
    fprintf(fp,"%i %.8lf %.8lf %.8lf ",z,
            rx[i]+(unfold?(ix[i]*L):0.0),
            ry[i]+(unfold?(iy[i]*L):0.0),
            rz[i]+(unfold?(iz[i]*L):0.0));
    if (put_vel)
      fprintf(fp,"%.8lf %.8lf %.8lf",vx[i],vy[i],vz[i]);
    fprintf(fp,"\n");
  }
}

void file_coordinates (FILE * fp,
              double * rx, double * ry, double * rz,
              double * vx, double * vy, double * vz,
              double L, int N, int n, double Sigma) {
  int i;
  double boxx= L * Sigma;
  boxx=boxx/Sigma;
  printf("Valor de boxx enviando a archivo =%f\n",boxx);
 //double boxx= L ;
  double boxix = 1.0f / boxx;

 for(int j=1;j<=N;j++)
  {
	rx[j] = rx[j]/Sigma;
	ry[j] = ry[j]/Sigma;
	rz[j] = rz[j]/Sigma;

  }

  fprintf(fp,"%f %f %f %f %f %f\n",boxx, boxx, boxx, boxix, boxix, boxix);
  for (i=1;i<=N;i++) {
    fprintf(fp,"%.8lf %.8lf %.8lf %.8lf %.8lf %.8lf ",
            rx[i],ry[i],rz[i],vx[i],vy[i],vz[i]);
    fprintf(fp,"\n");
  }
}

int xyz_in (FILE * fp, double * rx, double * ry, double * rz,
             double * vx, double * vy, double * vz,
             int * N) {
  int i;
  int has_vel, dum;
  fscanf(fp,"%i %i\n\n",N,&has_vel);
  for (i=0;i<(*N);i++) {
    fscanf(fp,"%i %lf %lf %lf ",&dum,&rx[i],&ry[i],&rz[i]);
    if (has_vel) { // read velocities
      fscanf(fp,"%lf %lf %lf",&vx[i],&vy[i],&vz[i]);
    }
  }
  return has_vel;
}


float gaussrand()
{
	static float V1, V2, S;
	static int phase = 0;
	float X;

	if(phase == 0) {
		do {
			float U1 = (float)rand() / RAND_MAX;
			float U2 = (float)rand() / RAND_MAX;

			V1 = 2 * U1 - 1;
			V2 = 2 * U2 - 1;
			S = V1 * V1 + V2 * V2;
			} while(S >= 1 || S == 0);

		X = V1 * sqrt(-2 * log(S) / S);
	} else
		X = V2 * sqrt(-2 * log(S) / S);

	phase = 1 - phase;

	return X;
}


/** \brief generate gaussian random number
 * generate a gaussian random number using the
 * Box-Muller method
 * \param[in] mean mean value of the gaussian
 * \param[in] sigma standard deviation of the gaussian
 * \return gaussian random number
 */
double drand_gaussian(double mean, double sigma) {
  static int i=0;
  static double x2;
  /* http://en.wikipedia.org/wiki/Box%E2%80%93Muller_transform */
  double r,theta,u1,u2,x1;
  if (i==0) {
    i=1-i;
    u1=1-drand48();
    u2=1-drand48();
    r=sqrt(-2*log(u1))*sigma;
    theta=2*M_PI*u2;
    x1 = r*cos(theta) + mean;
    x2 = r*sin(theta) + mean ;
    return(x1);
  } else {
    i=1-i;
    return(x2);
  }
}

/* Initialize particle positions by assigning them
   on a cubic grid, then scaling positions
   to achieve a given box size and thereby, volume,
   and density */
void init ( double * rx, double * ry, double * rz,
            double * vx, double * vy, double * vz,
            int * ix, int * iy, int * iz,
            int N, double * L, double T0,
            double * KE, char * icf, int n) {
  int i,iix,iiy,iiz;
  double cmvx=0.0,cmvy=0.0,cmvz=0.0;
  double T, fac;
  int n3=2;
  int vel_ok=0, addparticle, j;
  float rij, rxij, ryij, rzij, rxp ,ryp, rzp;
 float boxx;


    /* Find the lowest perfect cube, n3, greater than or equal to the
       number of particles */
/* Find the lowest perfect cube, n3, greater than or equal to the
       number of particles 
    while ((n3*n3*n3)<N) n3++;

    iix=iiy=iiz=0;
    /* Assign particle positions 
    for (i=0;i<N;i++) {
      rx[i] = ((double)iix+0.5)*L/n3;
      ry[i] = ((double)iiy+0.5)*L/n3;
      rz[i] = ((double)iiz+0.5)*L/n3;
      iix++;
      if (iix==n3) {
        iix=0;
        iiy++;
        if (iiy==n3) {
          iiy=0;
          iiz++;
        }
      }
    }
*/
    float volm = Massmolar/rho;
    printf("Volumen molar cm^3/mol = %0.16f\n", volm);
    float boxx_cm = pow((N*volm/AVOG),(1.0/3.0));
    printf("Lx cm = %0.16f\n", boxx_cm);
    boxx = boxx_cm / (Sigma * 1.0e-8);
    printf("Reduced Length side = %f\n", boxx);
    float vol = pow(boxx,3);

    *L=boxx*Sigma;
    printf("Length side in Argstrons = %f\n", *L);
    float boxxred = *L/Sigma;
    rho=N/vol;
    printf("Reduced density =%f\n", rho);
//    float vol=boxxred**3;  
    double Lx = *L /(float)n;
//    double Lx = L;
    double Lx2 = 0.5 * Lx;
    printf("L en init=%f\n",*L);
    printf("Lx en init=%f\n",Lx);
    printf("Lx2 en init=%f\n",Lx2);
    //Build the unit cell


   
 //Sublattice A
    rx[1]=0.0; ry[1]=0.0; rz[1]=0.0;
    //Sublattice A
    rx[2]=Lx2; ry[2]=Lx2; rz[2]=0.0;
    //Sublattice C
    rx[3]=0.0; ry[3]=Lx2; rz[3]=Lx2;
    //Sublattice D
    rx[4]=Lx2; ry[4]=0.0; rz[4]=Lx2;

    //Construct the lattice from de unit cell
    int m=0;
    for(int iz=1; iz<=n;iz++)
    {
	for(int iy=1; iy<=n;iy++)
	{
		for(int ix=1; ix<=n; ix++)
		{
			for(int iref=1; iref<=4; iref++)
			{
				rx[iref+m] = rx[iref] + Lx * (float)(ix-1);
				ry[iref+m] = ry[iref] + Lx * (float)(iy-1);
				rz[iref+m] = rz[iref] + Lx * (float)(iz-1);
			}
			m=m+4;
		}
	}
     }

    printf("Asignando 1 para cajas en la X, Y, Z direccion..\n");
    int nn=0;
    int ncellsx=1;
    for(int jx=0;jx<=ncellsx-1; jx++) 
    {
	for(int jy=0; jy<=ncellsx-1; jy++)
	{
		for(int jz=0; jz<=ncellsx-1; jz++)
		{
			for(int i=1;i<=N;i++)
			{
				nn++;
				rx[nn] = rx[i] + (float)(jx)* *L;
				ry[nn] = ry[i] + (float)(jy)* *L;
				rz[nn] = rz[i] + (float)(jz)* *L;
			}
		}
	}
    }
    
   *L = (float)ncellsx*boxx;
   printf("The new box is %f %f %f\n", *L, *L, *L);

				
	
// Inicializando velocidades
 


  float rtemp=sqrt(2.0/1);
  
  // If no velocities yet assigned, randomly pick some 
  if (!vel_ok) {
    for (i=1;i<=N;i++) {
      vx[i]=rtemp*gaussrand();
      vy[i]=rtemp*gaussrand();
      vz[i]=rtemp*gaussrand();
    }
  }
  //Remove net momentum
  float sumx=0, sumy=0, sumz=0;
  for (int i=1; i<= N; i++)
  {
	sumx=sumx+vx[i]*1;
	sumy=sumy+vy[i]*1;
	sumz=sumz+vz[i]*1;
  }
  sumx=sumx/(float)N;
  sumy=sumy/(float)N;
  sumz=sumz/(float)N;
  float uk=0;
  for (int i=1; i<=N; i++)
  {
	vx[i]=vx[i]-sumx;
	vy[i]=vy[i]-sumy;
	vz[i]=vz[i]-sumz;
	uk = uk + 1 * ( vx[i]*vx[i] + vy[i]*vy[i] + vz[i]*vz[i] );
  }

/*
 if (!vel_ok) {
    for (i=1;i<=N;i++) {
      vx[i]=drand_gaussian(0.0,1.0);
      vy[i]=drand_gaussian(0.0,1.0);
      vz[i]=drand_gaussian(0.0,1.0);
    }
  }
  /* Take away any center-of-mass drift; compute initial KE 
  for (i=0;i<*N;i++) {
    cmvx+=vx[i];
    cmvy+=vy[i];
    cmvz+=vz[i];
  }
  (*KE)=0;
  for (i=0;i<*N;i++) {
    vx[i]-=cmvx/(*N);
    vy[i]-=cmvy/(*N);
    vz[i]-=cmvz/(*N);
    (*KE)+=vx[i]*vx[i]+vy[i]*vy[i]+vz[i]*vz[i];
  }
  (*KE)*=0.5;
   if T0 is specified, scale velocities 
  if (T0>0.0) {
    T=(*KE)/(*N)*2./3.;
    fac=sqrt(T0/T);
    (*KE)=0;
    for (i=0;i<*N;i++) {
      vx[i]*=fac;
      vy[i]*=fac;
      vz[i]*=fac;
      (*KE)+=vx[i]*vx[i]+vy[i]*vy[i]+vz[i]*vz[i];
    }
    (*KE)*=0.5;
  }
  Initialize periodic boundary crossing counter arrays 
  for (i=0;i<*N;i++) {
    ix[i]=0;
    iy[i]=0;
    iz[i]=0;
  }*/
}

double ranf0()
{
  int L=1029, C = 221591, M = 1048576;
  int seed=0;
  double result;
  seed = (seed*L+C) % M;

  result = (float)seed / M;
//  printf("result=%f\n", result);
  return result;
}


double gauss()
{
  double A1 = 3.949846138, A3 = 0.252408784, A5 = 0.076542912, A7 = 0.008355968 , A9 = 0.029899776 ;
  double sum, r, r2;
  double result, temp;
  sum=0;
  for(int i=1; i<=12; i++)
  {
	temp = ranf0();
//	printf("temp de ranf0 =%f\n",temp);
  	sum = sum + temp;
  }
//  printf("sum=%f\n",sum);
  r = (sum - 6.0) / 4.0;
  r2 = r*r;
  result = (((( A9 * r2 + A7 ) * r2 + A5 ) * r2 + A3 ) * r2 +A1 ) * r;
//  printf("result de gauus=%f\n", result);
  return result;

}

void write_etas()
{
	const char filename[] = "fort.107";
	FILE *file = fopen(filename, "w");
	int m=0;
	printf("Saving etas on %s\n", filename);

	for (int mi=1; mi<=N; mi++)
	{
		m=m+1;
		for (int i=1;i<=NCH;i++)
		{
			fprintf(file, "%4.15f %4.15f \n", eta[i][m], etadot[i][m]);
		}
		//fprintf(file, "***\n");
	}

}



/* Definiendo rutina para ETAs */
void etas()
{
  double dofx, temp;
 /* T = T / Epsilon;
  printf("T=%f\n",T);
  amasssi = Mass * 1.0e-3 / AVOG;
  sigmasi = Sigma * 1.0e-10;
  epssi = Epsilon * BOLTZ;
  double ps = 1.0e-12;
  double tconst2 = ps * sqrt(epssi/amasssi) / sigmasi;
  Tau = Tau * tconst2;
*/
  int m=0;

  //Calculando los vectores
  for(int mi=1; mi<= N; mi++)
  { 
    m++;
    dofx = 3.0 * 1;
    //Se asume que shake==false
    for(int i=1; i<=NCH; i++)
    {
 	q[i][m] = T * Tau * Tau;
//	printf("q[i][m]=%f\n",q[i][m]);
	eta[i][m] = 0.0;
//	printf("temp=%f\n",temp);
//	etadot[i][m] = sqrt( T / q[i][m] ) * drand_gaussian(0.0,1.0);
	etadot[i][m] = sqrt( T / q[i][m] ) * gaussrand();
//	printf("etadot[i][m]=%f\n",etadot[i][m]);
    }
    etadot[2][m] = -etadot[1][m];
    q[1][m] = dofx * q[2][m];
  }

  write_etas();
  
 
}

int main ( int argc, char * argv[] ) {




  /* Here we parse the command line arguments;  If
   you add an option, document it in the usage() function! */
  for (i=1;i<argc;i++) {
    if (!strcmp(argv[i],"-n")) n=atoi(argv[++i]);
   // else if (!strcmp(argv[i],"-N")) N=atof(argv[++i]);
    else if (!strcmp(argv[i],"-rho")) rho=atof(argv[++i]);
    else if (!strcmp(argv[i],"-s")) Sigma=atof(argv[++i]);
    else if (!strcmp(argv[i],"-e")) Epsilon=atof(argv[++i]);
    else if (!strcmp(argv[i],"-m")) Mass = atof(argv[++i]);
    else if (!strcmp(argv[i],"-mm")) Massmolar=atof(argv[++i]);
    else if (!strcmp(argv[i],"-dt")) dt=atof(argv[++i]);
    else if (!strcmp(argv[i],"-T")) T=atof(argv[++i]);
    else if (!strcmp(argv[i],"-Tau")) Tau=atof(argv[++i]);

    else if (!strcmp(argv[i],"-sf")) wrt_code_str = argv[++i];
    else if (!strcmp(argv[i],"-icf")) init_cfg_file = argv[++i];
    else if (!strcmp(argv[i],"-ecorr")) use_e_corr = 1;
    else if (!strcmp(argv[i],"-seed")) Seed = (unsigned long)atoi(argv[++i]);
    else if (!strcmp(argv[i],"-uf")) unfold = 1;
    else if ((!strcmp(argv[i],"-h"))||(!strcmp(argv[i],"--help"))) {
      fprintf(stdout,"configHIMD usage:\n");
      fprintf(stdout,"configHIMD [options]\n\n");
      fprintf(stdout,"Options:\n");
      fprintf(stdout,"\t -n [integer]\t\tNumber of cells (default %i)\n",n);
      //fprintf(stdout,"\t -N [integer]\t\tNumber of atoms (default %i)\n",N);
      fprintf(stdout,"\t -rho [real]\t\tNumber density gr/cm3(default %f)\n",rho);
      fprintf(stdout,"\t -dt [real]\t\tTime (default %f)\n",dt);
      fprintf(stdout,"\t -s [real]\t\tSigma parameter reference (default %f)\n",Sigma);
      fprintf(stdout,"\t -e [real]\t\tEpsilon parameter reference (default %f)\n",Epsilon);
      fprintf(stdout,"\t -m [real]\t\tMass parameter reference (default %f)\n",Mass);
      fprintf(stdout,"\t -mm [real]\t\tMassmolar parameter reference (default %f)\n",Massmolar);

      fprintf(stdout,"\t -T [real]\t\tTemperature (default %f)\n",T);
      fprintf(stdout,"\t -Tau [real]\t\tTau (default %f)\n",Tau);

      fprintf(stdout,"\t -sf [a|w]\t\tAppend or write config output file (default %s)\n",wrt_code_str);
      fprintf(stdout,"\t -seed [integer]\tRandom number generator seed (default %li)\n",Seed);
      fprintf(stdout,"\t -h           \t\tPrint this info\n");
      exit(0);
    }
    else {
      fprintf(stderr,"Error: Command-line argument '%s' not recognized.\n",
              argv[i]);
      exit(-1);
    }
  }

  fprintf(stdout,"# Config build coordinates, velocities and etas for HIMD\n");

  //Variables to reduced units
//  amasssi = Mass * 1.0e-3 / AVOG;
  //amassmolarsi = Massmolar * 1.0e-3 / AVOG;
  //sigmasi = Sigma * 1.0e-10;
  //rhoconst = pow(sigmasi,3)/amassmolarsi;

  //Calculate the side oh the unit cell
  N = 4 * pow(n,3);
  fprintf(stdout,"# Numero de celdas %d\n", n);
  fprintf(stdout,"# Numero de atomos %d\n", N);

  //Volumen molar
  fprintf(stdout,"# Rho %.5lf\n", rho);

  /* Compute the side-length */
  //rhosi = rho * 1.0e+3;
  //rho = rhosi * rhoconst;
  //L = pow( ((float)N / rho), (1.0/3.0) );
//  L = pow((N / rho),(1.0/3.0));
 // fprintf(stdout,"# Reduce Length side %.5lf\n", L);

  fprintf(stdout,"# Reduced density %.5lf\n", rho);
//  fprintf(stdout,"# Length side in Armsgtrongs %.5lf\n", L);
  fprintf(stdout,"# Temperature %.5lf\n", T);
  fprintf(stdout,"# Tau %.5lf\n", Tau);



  /* Seed the random number generator */
  srand48(Seed);

  /* Allocate the position arrays */
  rx = (double*)malloc(N*sizeof(double));
  ry = (double*)malloc(N*sizeof(double));
  rz = (double*)malloc(N*sizeof(double));

  /* Allocate the boundary crossing counter arrays */
  ix = (int*)malloc(N*sizeof(int));
  iy = (int*)malloc(N*sizeof(int));
  iz = (int*)malloc(N*sizeof(int));

  /* Allocate the velocity arrays */
  vx = (double*)malloc(N*sizeof(double));
  vy = (double*)malloc(N*sizeof(double));
  vz = (double*)malloc(N*sizeof(double));




  /* Generate initial positions on a cubic grid */
  printf("Generando posiciones y velocidades...\n");
  init(rx,ry,rz,vx,vy,vz,ix,iy,iz,N,&L,T0,&KE,init_cfg_file,n);
  sprintf(fn,"%i.xyz",0);
  printf("Escribiendo archivo xyz para vmd...\n");
  out=fopen(fn,"w");
  xyz_out(out,rx,ry,rz,vx,vy,vz,ix,iy,iz,L,N,16,1,unfold);
  fclose(out);
  printf("Escribiendo archivo de coordenadas para himd...\n");
  out=fopen("fort.2","w");
  file_coordinates(out,rx,ry,rz,vx,vy,vz,L,N,n, Sigma);
  printf("Escrihiendo archivo etas para himd...\n");
  etas();



  
  
  free(rx);
  free(ry);
  free(rz);
  free(vx);
  free(vy);
  free(vz);
  free(ix);
  free(iy);
  free(iz);
  return(0);
}
