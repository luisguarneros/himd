# HIMD

HIMD is a Molecular dynamics simulation software with the implementation of time integration on Nosé-Hoover chains (NHC) on GPU.


## Add your files

- [ ] [Create](https://gitlab.com/-/experiment/new_project_readme_content:9251e96a373aad58787cd55d7c8111f1?https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://gitlab.com/-/experiment/new_project_readme_content:9251e96a373aad58787cd55d7c8111f1?https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://gitlab.com/-/experiment/new_project_readme_content:9251e96a373aad58787cd55d7c8111f1?https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/luisguarneros/himd.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/-/experiment/new_project_readme_content:9251e96a373aad58787cd55d7c8111f1?https://docs.gitlab.com/ee/user/project/integrations/)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://gitlab.com/-/experiment/new_project_readme_content:9251e96a373aad58787cd55d7c8111f1?https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://gitlab.com/-/experiment/new_project_readme_content:9251e96a373aad58787cd55d7c8111f1?https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://gitlab.com/-/experiment/new_project_readme_content:9251e96a373aad58787cd55d7c8111f1?https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Automatically merge when pipeline succeeds](https://gitlab.com/-/experiment/new_project_readme_content:9251e96a373aad58787cd55d7c8111f1?https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://gitlab.com/-/experiment/new_project_readme_content:9251e96a373aad58787cd55d7c8111f1?https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://gitlab.com/-/experiment/new_project_readme_content:9251e96a373aad58787cd55d7c8111f1?https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://gitlab.com/-/experiment/new_project_readme_content:9251e96a373aad58787cd55d7c8111f1?https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://gitlab.com/-/experiment/new_project_readme_content:9251e96a373aad58787cd55d7c8111f1?https://docs.gitlab.com/ee/user/clusters/agent/)

***

## Name
HIMD: High-throughput of measure-preserving integrators derived from the Liouville operator for molecular dynamics simulations on GPUs.

## Description
Molecular dynamics simulation is currently a theoretical technique eligible in simulating of a wide range of systems, from soft condensed matter to biological systems. Excellent outcomes have resulted from using this technique; however, the implementation of this approach remains computationally expensive to some extent. Novel computing technologies may help reducing the computational simu-lation time, particularly, by using Graphical Processing Units (GPUs). Calcula-tions on GPUs make possible to carry out simulations of large and complex mo-lecular systems at a significantly reduced time. In this manuscript, the implemen-tations of measure-preserving geometric integrator in the canonical ensemble cod-ed in Compute Unified Device Architecture (CUDA) language are presented. The performance and validation of our High-throughput Molecular Dynamics (HIMD) code was done by calculating the thermodynamic properties of a Len-nard-Jones fluid. From our tested systems, an excellent agreement was achieved with the reported of literature, compared with calculations carried out on Central Processing Units (CPUs). The implementation of the HIMD code performs time integration on Nosé-Hoover chains (NHC) faster in comparison to the NHC method implemented in LAMMPS code tested with one CPU vs one GPU. Along this work, the scope and limitations in performing simulations by using our HIMD code under rigorous statistical thermodynamics in the canonical en-semble are discussed and analyzed.


## Installation
To install HIMD on your computer, just download the particle version to simulate along with the setup.txt file and place it in some folder inside your account on your Linux system.

## Usage
For its execution you will need a Linux system with NVidia GPU card and CUDA 11.
Example of execution

./himd_2048 setup.txt

## Support
For support you can send an email to the following address:

luisguarneros at gmail.com


## Authors and acknowledgment
L.G.N. acknowledges CONACyT México for supporting his postdoctoral stay at the Tecnológico Nacional de México.

## License
Use freedom.

## Project status
On development.

